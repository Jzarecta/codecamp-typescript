interface ITodo {
  task: string;
  descr: string;
  done: boolean;
}

class Todo implements ITodo {
  constructor(public task: string, public descr: string, public done: boolean) {

  }
}

class TodoList {
  public static todos: Todo[] = [];

  public addTodo(task: string, descr: string): Todo {
    let todo = new Todo(task, descr, false);
    TodoList.todos.push(todo);
    return todo;
  }
}

window.onload = function () {
  let task = <HTMLInputElement> document.getElementById('task');
  let descr = <HTMLInputElement> document.getElementById('descr');
    
  let button = <HTMLButtonElement>document.getElementById('btnAdd'); 
  button.addEventListener('click', () => addTask(task.value, descr.value));
}

function addTask(task: string, descr: string) {
  
  let obj = new TodoList();
  let todo = obj.addTodo(task, descr);

  let list = <HTMLUListElement> document.getElementById('todoList');
  let item = <HTMLLIElement> document.createElement('li');
  
  item.innerHTML = `${todo.task}: ${todo.descr}`;
  item.classList.add('list-group-item');
  item.addEventListener('click', () => doneTask(item));

  list.appendChild(item);

  // Limpiar inputs
  (<HTMLInputElement> document.getElementById('task')).value = '';
  (<HTMLInputElement> document.getElementById('descr')).value = '';
}

function doneTask(item: HTMLLIElement) {
  item.classList.add('todoDone');
}