"use strict";
var Todo = /** @class */ (function () {
    function Todo(task, descr, done) {
        this.task = task;
        this.descr = descr;
        this.done = done;
    }
    return Todo;
}());
var TodoList = /** @class */ (function () {
    function TodoList() {
    }
    TodoList.prototype.addTodo = function (task, descr) {
        var todo = new Todo(task, descr, false);
        TodoList.todos.push(todo);
        return todo;
    };
    TodoList.todos = [];
    return TodoList;
}());
window.onload = function () {
    var task = document.getElementById('task');
    var descr = document.getElementById('descr');
    var button = document.getElementById('btnAdd');
    button.addEventListener('click', function () { return addTask(task.value, descr.value); });
};
function addTask(task, descr) {
    var obj = new TodoList();
    var todo = obj.addTodo(task, descr);
    var list = document.getElementById('todoList');
    var item = document.createElement('li');
    item.innerHTML = todo.task + ": " + todo.descr;
    item.classList.add('list-group-item');
    item.addEventListener('click', function () { return doneTask(item); });
    list.appendChild(item);
    // Limpiar inputs
    document.getElementById('task').value = '';
    document.getElementById('descr').value = '';
}
function doneTask(item) {
    item.classList.add('todoDone');
}
