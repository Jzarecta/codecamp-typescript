# Taller Introducción a Typescript              
Todo app para el taller de introducción a Typescript

# Instalación

Ejecutar el comando ```npm -g install typescript``` y ```tsc --init ``` en el directorio del proyecto. 

Crear los archivos app.ts

# Reporte de Debug
Abrir una terminal nueva y ejecutar la linea ```tsc -p ./ -w``` esto lanzara mensajes de compilación del app.ts al nuevo app.js
